# Use of Git

Quick reference for the most common git commands.

### Connecting your local workspace to the remote server

Read details on how to configure connection through SSH in [Authentication](https://gitlab.com/sprava/use-of-git/-/blob/master/Authentication.md)

### Configuration commands

If something is not configured that should be, **git** will kindly tell you what and how after unsuccessful try to commit. Nevertheless, here are the most frequent of those configuring commands:

* <code>git config user.name <*username*></code>
* <code>git config user.email <*your_email*></code>

Some other useful commands:

* <code>git config core.editor <*your_favorite_text_editor*></code>
<br/>This sets the default text editor that pops up when You make a commit, e.g. `nano` or `vi`.
* <code>git config help.autoCorrect <*integer*></code>
<br/>This enables (with positive parameter) or disables (with parameter of zero) executing autocorrected commands. By default **git** only shows autocorrected variants, but with this feature enabled it will wait some short time and then execute the command if You didn't interrupt it. The parameter here is tenth of second that **git** will wait for, e.g. *42* means *4.2* seconds.

All these configurations are made with default key `--local`, i.e. for the current repository only. With key `--global` You can apply changes globally.

To see more configurable parameters You can start typing `git config abc` and use autocompletion feature to your advantage. Also, read `man git-config`.

By the way, command <code>git config <*variable*></code> without a parameter shows the current value (or nothing if variable is unset).

### The most frequent commands

Usual sequence to make a commit:

0. `git status`
<br/>See the current state of your repository: current branch and position (the last commit), which files are modified, which files are prepared for commit, untracked files, etc.
1. <code>git add *\<file\> \[more files\]*</code>
<br/>Add files that were changed, removed or added to the staging area (preparation for committing).
2. `git status`
<br/>It's never bad idea to check.
3. `git commit`
<br/>An actual commit. **git** will open the text editor where you can input message that describes what is your commit for. If you want to cancel commit, leave empty message and close the text editor. Alternatively, You can provide the message with key `-m`, e.g.
<br/>`git commit -m "A very important commit"`
<br/>Another useful key is `--amend`. With this key You can replace the previous commit; of course this is useful as long as that previous commit is only saved locally, i.e. You have not pushed it yet.
4. `git push`
<br/>Send your commit(s) to the remote server.

To retrieve the current state of the repository on the remote server, use the command
* `git pull`

To switch to another branch:
* <code>git checkout *\<branch name\>*</code>
<br/>Without a name **git** will switch to *master* branch.

To create a new branch You can either
* <code>git branch *\<new branch's name\>*</code>
* <code>git checkout -b *\<new branch's name\>*</code>
<br>This latter command also switches to a new branch.

With the command `git branch` You can also see all the branches in your local repositiry (when without keys), remove a branch (with `-d` key).

### Merge... and conflicts

Instructions to cope with conflicts when merging.

[//]: # (TODO!!!)

### Other useful commands

* `git --version`

* `git log`
<br/>Look at the last commits. You can specify the number of commits, e.g. `git log -3` lists the last three commits.
<br/>To fit into one line, provide `--oneline` or `--pretty=oneline`. With `--pretty=` You can specify a very specific format of output.
