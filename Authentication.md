# Authentication

To connect (through SSH) your local workspace and the remote server, some configurations are required:
1. generate a new key (or use an existing one),
2. make server know your (public) key,
3. configure `git`.

### Generate a new key

Better read dedicated instructions on a specific server, e.g. on <https://gitlab.com> or <https://github.com>, but generally You'll do the following:

* Create public/private keys pair in `.ssh` folder using command
<code>ssh-keygen *[some options]* -C "<*your_email*>"</code>
Options could be `-t rsa -b 2048` (2048-bits-wide RSA key) or `-t ed25519` (ED25519 key) etc, depending on what is supported by the server of your choice.
* Done! But check that the new key was added automatically:
<code>ssh-add -l</code>

### Make server know your public key

Follow the instructions of a specific server. Most likely You'll be requested to put the text from the public key file into some text box under *Settings/SSH* section.

### Configure git

Add an entry into `.ssh/config` file. E.g. for <https://gitlab.com> it would be:
```
Host gitlab.com
    HostName gitlab.com
    User git
    IdentityFile ~/.ssh/private_key
```
You can make multiple entries for different servers and even for different accounts (at least with [githun](https://github.com) and [gitlab](https://gitlab.com) You can specify a username, e.g. `Host gitlab.com-username`). With such configuration, there are good chances that when you're cloning a repository via `git clone ...` the correct keys will be chosen automatically based on the repository address.
